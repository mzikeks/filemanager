﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileManager
{
    // Методы, начинающиеся с Is*метод* проверяют, соответствует ли ввод (количество аргументов) методу.
    // Методы, начинающиеся с Execute*метод* запускаются из Is... и выполняют какое-то действие.
    // Каждый ввод пользователся программа с начала получает в Main, проверяет его на exit, затем оправляет его в TryParseCommands.
    // Там запускаются все методы проверки ввода на соответстве их каждой из команд.
    // Метод Is... разбирает ввод на аргументы, проверяет их на корректность и запускает метод Execute..., в котором выпоняется действие
  
    class Program
    {
        // Приветственное сообщение, цикл приема ввода пользователя, проверка на выход.
        static void Main(string[] args)
        { 
            Console.WriteLine($"Добро пожаловать в файловый менеджер!" + Environment.NewLine +
                $"Сейчас вы находитесь в каталоге {Directory.GetCurrentDirectory()}" + Environment.NewLine +
                $"Список доступных команд вы всегда можете посмотреть при помощи команды help");

            ExecuteHelp();

            while (true)
            {
                Console.Write($"{Directory.GetCurrentDirectory()}>");
                string input = Console.ReadLine();

                if (IsExit(input))
                {
                    break;
                }
                TryParseCommands(input);
                Console.WriteLine();

            }

        }

        // Проверка ввода пользователя на выход.
        static bool IsExit(string input)
        {
            return (input.Trim().ToLower() == "exit") ? true : false;
        }

        // Сюда попадает любой ввод пользователя.
        static void TryParseCommands(string input)
        {
            try
            {
                // Переменная отвечает за то, существует ли введенная команда.
                bool isCommand = false;

                // Проверяем, соответствует ли ввод каждой из доступных команд.
                IsCorrectHelp(input, ref isCommand);
                IsCorrectClear(input, ref isCommand);
                IsCorrectListDrives(input, ref isCommand);
                IsCorrectChangeDirectory(input, ref isCommand);
                IsCorrectMove(input, ref isCommand);
                IsCorrectCopy(input, ref isCommand);
                IsCorrectDelete(input, ref isCommand);
                IsCorrectConcatenateTextFiles(input, ref isCommand);
                IsCorrectMakeFile(input, ref isCommand);
                IsCorrectReadFile(input, ref isCommand);
                IsCorrectMakeDirectory(input, ref isCommand);
                IsCorrectShowObjects(input, ref isCommand);

                if (!isCommand)
                {
                    Console.WriteLine("О нет! Кажется, такой команды не существует. Проверьте ваш ввод и попробуйте ещё раз. Введите help для помощи");
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Возможно, вы делаете что-то не так " + e.Message);
            }
            
        }

        // clear: Метод, очищающий консоль.
        private static void ExecuteClear()
        {    
            Console.Clear();
        }


        // help: Метод, печатающий список доступных команд.
        private static void ExecuteHelp()
        {
            Console.WriteLine();
            Console.WriteLine(
                "Важная информация: для указания имен с пробелами испольуйте кавычки: \"123 abc\" " + Environment.NewLine +
                "Другое использование кавычек не допускается!" + Environment.NewLine + Environment.NewLine +
                "Доступные команды:" + Environment.NewLine +
                "help \t\t вывод списка доступных команд" + Environment.NewLine+
                "clear \t\t очистить консоль" + Environment.NewLine +
                "ld \t\t - посмотреть список доступных дисков" + Environment.NewLine +
                "cd \t\t - выбрать директорию (папку) или диск" + Environment.NewLine +
                "mv *from* *to* \t - переместить файл из a в b" + Environment.NewLine +
                "cp *from* *to* \t - скопироватm файл/папку из a в b" + Environment.NewLine +
                "del *path* \t - удалить файл/папку" + Environment.NewLine +
                "mkdir *name* \t - создать папку" + Environment.NewLine +
                "exit \t\t - выход" + Environment.NewLine +
                "conc  *file1* *...* *file2* *file_res* \t - объединить текстовые файлы file1...file_n (UTF8) в новый файл file_res" + Environment.NewLine + Environment.NewLine +
                "mkfile *name* \t - создать текстовый файл (возможно задать кодировку через -*encoding*)" + Environment.NewLine +
                "rd *name* \t - прочитать текстовый файл (возможно задать кодировку через -*encoding*)" + Environment.NewLine +
                "\t\t   (доступные кодировки: uft8 (по умолчанию), utf32, unicode, ascii)" + Environment.NewLine + Environment.NewLine +
                "ls \t\t - просмотр папок и файлов в текущей директории " + Environment.NewLine +
                "\t\t   (флаг -d только для директорий, флаг -f только для файлов)"
            );
            Console.WriteLine();
            Console.WriteLine();
        }

        // ls: Метод печатает содержимое папки. type отвечает за флаги, который пользователь указал при вводе.
        // 0 - файлы и папки. 1 - только файлы, -1 - только папки.
        private static void ExecuteShowObjects(int type)
        {
            string directory = Directory.GetCurrentDirectory();
            string[] directoryName = (directory).Split(Path.DirectorySeparatorChar); 
            Console.WriteLine($"Содержимое папки {directoryName[directoryName.Length-1]}");

            if (type == 1 || type == 0)
            {
                foreach (string file in Directory.GetFiles(directory))
                {

                    Console.WriteLine($"<FILE> {file.Substring(file.LastIndexOf(Path.DirectorySeparatorChar) + 1)}");
                }
            }
            if (type == -1 || type == 0)
            {
                foreach (string subDirectory in Directory.GetDirectories(Directory.GetCurrentDirectory()))
                {
                    Console.WriteLine($"<DIR> {subDirectory.Substring(subDirectory.LastIndexOf(Path.DirectorySeparatorChar) + 1)}");
                }
            }
        }

        // mkfile: Создает новый файл по указанному пути.
        private static void ExecuteMakeFile(string path, string text, Encoding encoding)
        {
            string directory = Path.GetDirectoryName(path);
            // Если пользователь указал путь до файла, в котором указаны еще несуществующие папки,
            // То будет ошибка, поэтому нужно создать все необходимые папки.
            
            try
            {
                if (directory.Length > 0 && !Directory.Exists(directory))
                {
                    ExecuteMakeDirectory(Path.GetDirectoryName(path));
                }

                
            }
            catch (Exception e)
            {
                Console.WriteLine("Что-то пошло не так. Возможно у вас недостаточно прав " + e.Message);
            }


            if (File.Exists(path))
            {
                Console.WriteLine("Файл с таким именем уже существует");
            }

            else
            {
                try
                {
                    File.WriteAllText(path, text, encoding);
                    Console.WriteLine("Файл успешно создан");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка! " + e.Message);
                }
            }
            
        }

        // rd: Метод читает файл и выводит его содержимое в консоль в указанной кодировке.
        private static void ExecuteReadFile(string path, Encoding encoding)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("По этому пути не удается найти файл с указанным именем");
            }
            else
            {
                try
                {
                    Console.WriteLine(File.ReadAllText(path, encoding));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка! " + e.Message);
                }
            }
        }

        // mkdir: Создает новую папку (если это возможно).
        private static void ExecuteMakeDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Console.WriteLine("Папка с таким именем уже существует");
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(path);
                    Console.WriteLine("Папка успешно создана");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка! " + e.Message);
                }
            }
        }

        // conc: производит конкатенацию данных файлов.
        private static void ExecuteConcatenateTextFiles(List<string> inputFilesNames, string resulFileName)
        {
            // Здесь будет храниться результирующая строка.
            var resultText = new StringBuilder();
            foreach (string inputFileName in inputFilesNames)
            {
                try
                {
                    if (!File.Exists(inputFileName))
                    {
                        Console.WriteLine($"Не могу найти файл {inputFileName}. Попробуйте снова");
                        return;
                    }
                    resultText.Append(File.ReadAllText(inputFileName, Encoding.UTF8));
                    resultText.Append(Environment.NewLine);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка во время конкатенации. Возможно, какие-то проблемы с файлами. " + e.Message);
                }
                
            }
            Console.WriteLine("Объединяю файлы...");
            ExecuteMakeFile(resulFileName, resultText.ToString(), Encoding.UTF8);
        }
        
        // del: Метод удаляет файл ИЛИ ПАПКУ.
        private static void ExecuteDelete(string path)
        {
            if (File.Exists(path) || Directory.Exists(path))
            {
                try
                {
                    File.Delete(path);
                    Console.WriteLine("Файл успешно удален");
                }

                catch (UnauthorizedAccessException) // Это означает что по указанному пути лежит не файл, а папка.
                {
                    try
                    {
                        Directory.Delete(path, true);
                        Console.WriteLine("Папка успешно удалена");
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Не удается получить доступ к файлу/папке. Возможно, у вас нет прав его просматривать" +
                                          " или файл выполняется другой программой");
                    }
                }
            }
            else
            {
                Console.WriteLine("Похоже, что файла и папки с таким именем не существует");
            }
            
        }

        // mv: Метод перемещает файл из одной папки from в другую папку to ИЛИ ПАПКУ ЦЕЛИКОМ.
        private static void ExecuteMove(string from, string to)
        {
            if (File.Exists(from))
            {
                try
                {
                    File.Move(from, to);
                    Console.WriteLine("Перемещение файла выполнено успешно!");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Ошибка! Перемещение не выполнено {e.Message}");
                }
            }
            else if (Directory.Exists(from))
            {
                try
                {
                    Directory.Move(from, to);
                    Console.WriteLine("Перемещение папки выполнено успешно!");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Ошибка! Перемещение не выполнено {e.Message}");
                }
            }
            else
            {
                Console.WriteLine("По указанному пути ничего не удалось найти");
            }
        }

        // cp: Метод копирует файл из from в to.
        private static void ExecuteCopy(string from, string to)
        {
            if (File.Exists(from))
            {
                try
                {
                    File.Copy(from, to);
                    Console.WriteLine("Копирование файла выполнено успешно!");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Ошибка! Копирование не выполнено {e}");
                }
            }
            else
            {
                Console.WriteLine("По указанному пути не удалось найти файл");
            }
        }

        // cd: Метод изменяет текущую директорию.
        private static void ExecuteChangeDirectory(string directory)
        {
            try
            {
                Directory.SetCurrentDirectory(directory);
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("Директория с указнанным именем не найдена");
            }
        }

        // ld: Метод печатает список доступных дисков и их типы.
        private static void ExecuteListDrive()
        {
            Console.WriteLine("Доступные диски:");
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady)
                {
                    Console.WriteLine($"Диск {drive.Name}, тип {drive.DriveType}");
                }
            }

        }



        // Проверяет, соответствует ли ввод команде ls.
        private static bool IsCorrectShowObjects(string input, ref bool isCommand)
        {
            string[] arguments = ParseArguments(input);
            if (arguments[0].ToLower() != "ls")
            {
                return false;
            }

            isCommand = true;

            // Дальше идет проверка флагов -f - только файлы -d только папки.
            if (arguments.Length == 1)
            {
                ExecuteShowObjects(0);
                return true;
            }
            else if (arguments.Length == 2 && arguments[1].ToLower() == "-f")
            {
                ExecuteShowObjects(1);
                return true;
            }
            else if (arguments.Length == 2 && arguments[1].ToLower() == "-d")
            {
                ExecuteShowObjects(-1);
                return true;
            }

            Console.WriteLine("Вы ошиблись с числом аргументов. Для метода ls ожидается один аргумент (флаг -f или -d)" +
                              "или 0 аргументов");
            return false;
        }

        // Проверяет, соответствует ли ввод команде mkfile.
        private static bool IsCorrectMakeFile(string input, ref bool isCommand)
        {
            string[] arguments = ParseArguments(input);
            if (arguments[0].ToLower() != "mkfile")
            {
                return false;
            }
           
            Encoding encoding = Encoding.UTF8;

            isCommand = true;
           
            if (arguments.Length == 3)
            {
                if (arguments[2].ToLower() == "-utf8")
                {
                    encoding = Encoding.UTF8;
                }
                else if (arguments[2].ToLower() == "-utf32")
                {
                    encoding = Encoding.UTF32;
                }
                else if (arguments[2].ToLower() == "-ascii")
                {
                    encoding = Encoding.ASCII;
                }
                else if (arguments[2].ToLower() == "-unicode")
                {
                    encoding = Encoding.Unicode;
                }
                else
                {
                    Console.WriteLine("Вы ввели неизветсную мне кодировку. Доступные кодировки: utf8, utf32, ascii, unicode");
                    return false;
                }
            }

            else if (arguments.Length != 2)
            {
                Console.WriteLine("Вы ошиблись с числом аргументов. Для метода mkfile ожидается один аргумент" +
                                  "- путь, по которому будет лежать будущий файл");
                return false;
            }
            ExecuteMakeFile(arguments[1], String.Empty, encoding);
            return true;
        }

        // Проверяет, соответствует ли ввод команде rd.
        private static bool IsCorrectReadFile(string input, ref bool isCommand)
        {
            string[] arguments = ParseArguments(input);
            if (arguments[0].ToLower() != "rd")
            {
                return false;
            }
            Encoding encoding = Encoding.UTF8;

            isCommand = true;

            if (arguments.Length == 3)
            {
                if (arguments[2].ToLower() == "-utf8")
                {
                    encoding = Encoding.UTF8;
                }
                else if (arguments[2].ToLower() == "-utf32")
                {
                    encoding = Encoding.UTF32;
                }
                else if (arguments[2].ToLower() == "-ascii")
                {
                    encoding = Encoding.ASCII;
                }
                else if (arguments[2].ToLower() == "-unicode")
                {
                    encoding = Encoding.Unicode;
                }
                else
                {
                    Console.WriteLine("Вы ввели неизветсную мне кодировку. Доступные кодировки: utf8, utf32, ascii, unicode");
                    return false;
                }
            }

            else if (arguments.Length != 2)
            {
                Console.WriteLine("Вы ошиблись с числом аргументов. Для метода rd ожидается один аргумент" +
                                  "- путь к файлу, который вы хотите прочитать");
                return false;
            }
            ExecuteReadFile(arguments[1], encoding);
            return true;
        }

        // Проверяет, соответствует ли ввод команде mkdir.
        private static bool IsCorrectMakeDirectory(string input, ref bool isCommand)
        {
            string[] arguments = ParseArguments(input);
            if (arguments[0].ToLower() != "mkdir")
            {
                return false;
            }

            isCommand = true;

            if (arguments.Length != 2)
            {
                Console.WriteLine("Вы ошиблись с числом аргументов. Для метода mkdir ожидается один аргумент - путь до новой папки");
                return false;
            }
            ExecuteMakeDirectory(arguments[1]);
            return true;
        }

        // Проверяет, соответствует ли ввод команде conc.
        private static bool IsCorrectConcatenateTextFiles(string input, ref bool isCommand)
        {
            string[] arguments = ParseArguments(input);
            if (arguments[0].ToLower() != "conc")
            {
                return false;
            }

            isCommand = true;

            if (arguments.Length < 4)
            {
                Console.WriteLine("Вы ошиблись с числом аргументов. Для метода con ожидается не менее трех аргументов " +
                                  "- пути к двум или нескольким файлам для конкатенации, и путь к результирующему файлу");
                return false;
            }

            var inputFilesNames = new List<string>();
            for (int i = 1; i < arguments.Length-1; i++)
            {
                inputFilesNames.Add(arguments[i]);
            }

            ExecuteConcatenateTextFiles(inputFilesNames, arguments[arguments.Length - 1]);
            return true;
        }

        // Проверяет, соответствует ли ввод команде del.
        private static bool IsCorrectDelete(string input, ref bool isCommand)
        {
            string[] arguments = ParseArguments(input);
            if (arguments[0].ToLower() != "del")
            {
                return false;
            }

            isCommand = true;

            if (arguments.Length != 2)
            {
                Console.WriteLine("Вы ошиблись с числом аргументов. Для метода del ожидается один аргумент " +
                                  "- путь к файлу или папке, которые нужно удалить");
                return false;
            }
            ExecuteDelete(arguments[1]);
            return true;
        }

        // Проверяет, соответствует ли ввод команде cp.
        private static bool IsCorrectCopy(string input, ref bool isCommand)
        {
            string[] arguments = input.Split();
            if (arguments[0].ToLower() != "cp")
            {
                return false;
            }

            isCommand = true;

            if (arguments.Length != 3)
            {
                Console.WriteLine("Вы ошиблись с числом аргументов. Для метода cp ожидается два аргумента " +
                                  "- путь к файлу, который нужно скопировать и путь, куда его нужно скопировать");
                return false;
            }
            ExecuteCopy(arguments[1], arguments[2]);
            return true;
        }

        // Проверяет, соответствует ли ввод команде mv.
        private static bool IsCorrectMove(string input, ref bool isCommand)
        {
            string[] arguments = ParseArguments(input);
            if (arguments[0].ToLower() != "mv")
            {
                return false;
            }

            isCommand = true;

            if (arguments.Length != 3)
            {
                Console.WriteLine("Вы ошиблись с числом аргументов. Для метода mv ожидается два аргумента " +
                                  "- путь к файлу, который нужно переместить и путь, куда его нужно переместить");
                return false;
            }
            ExecuteMove(arguments[1], arguments[2]);
            return true;
        }

        // Проверяет, соответствует ли ввод команде cd.
        private static bool IsCorrectChangeDirectory(string input, ref bool isCommand)
        {
            string[] arguments = ParseArguments(input);
            if (arguments[0].ToLower() != "cd")
            {
                return false;
            }

            isCommand = true;

            if (arguments.Length != 2)
            {
                Console.WriteLine("Вы ошиблись с числом аргументов. Для метода cd ожидается один аргумент - папка в которую нужно перейти");
                return false;
            }
            ExecuteChangeDirectory(arguments[1]);
            return true;
        }

        // Проверяет, соответствует ли ввод команде ld.
        private static bool IsCorrectListDrives(string input, ref bool isCommand)
        {
            if (input.Trim().ToLower() == "ld")
            {
                isCommand = true;
                ExecuteListDrive();
                return true;
            }
            return false;
        }

        // Проверяет, соответствует ли ввод команде help.
        private static bool IsCorrectHelp(string input, ref bool isCommand)
        {
            if (input.Trim().ToLower() == "help")
            {
                isCommand = true;
                ExecuteHelp();
                return true;
            }
            return false;
        }

        // Проверяет, соответствует ли ввод команде clear.
        private static bool IsCorrectClear(string input, ref bool isCommand)
        {
            if (input.Trim().ToLower() == "clear")
            {
                isCommand = true;
                ExecuteClear();
                return true;
            }
            return false;

        }

        // Задача этого метода - разбить строку на массив аргуметов по пробельным символам.
        // Сложность заключается в том, что пробельные символы внутри кавычек учитывать не нужно.
        private static string[] ParseArguments(string input)
        {
            var arguments = new List<string>();
            var inputPart = new StringBuilder();
            bool inQuote = false;

            foreach (char letter in input)
            {
                if (letter == '"')
                {
                    if (inQuote)
                    {
                        arguments.Add(inputPart.ToString());
                    }
                    else
                    {
                        foreach (string argument in inputPart.ToString().Split())
                        {
                            if (argument.Length != 0)
                            {
                                arguments.Add(argument);
                            }
                        }
                    }
                    inQuote = !inQuote;
                    inputPart.Clear();
                }
                else
                {
                    inputPart.Append(letter);
                }
            }

            foreach (string argument in inputPart.ToString().Split())
            {
                if (argument.Length != 0)
                {
                    arguments.Add(argument);
                }
            }
            return arguments.ToArray();
        }
    }
}
